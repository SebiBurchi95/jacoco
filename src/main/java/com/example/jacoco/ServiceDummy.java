package com.example.jacoco;

import org.springframework.stereotype.Service;

@Service
public class ServiceDummy {

    int sum(int a, int b) {
        return a + b;
    }

    int increase(int a) {
        return a++;
    }

    int decrease(int a) {
        return a--;
    }
}
