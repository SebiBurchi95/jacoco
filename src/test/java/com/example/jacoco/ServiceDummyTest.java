package com.example.jacoco;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ServiceDummyTest {

    @Autowired
    private ServiceDummy serviceDummy;

    @Test
    void testSum() {
        assertEquals(7, serviceDummy.sum(3, 4));
    }

}
